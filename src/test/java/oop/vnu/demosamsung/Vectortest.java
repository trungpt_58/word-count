/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oop.vnu.demosamsung;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Trung Arsenal
 */
public class Vectortest {
    
    @Test
    public void testAddVector(){
        Vector v1 = new Vector();
        v1.x = 1;
        v1.y = 2;
        Vector v2 = new Vector();
        v2.x = 3;
        v2.y = 4;
        Vector sum = v1.add(v2);
        assertEquals(4,sum.x);
        assertEquals(6,sum.y);
        
    }
    @Test
    public void testSubtractVector(){
         Vector v1 = new Vector();
        v1.x = 1;
        v1.y = 2;
        Vector v2 = new Vector();
        v2.x = 3;
        v2.y = 4;
        Vector sub = v1.subtract(v2);
        
        assertEquals(-2,sub.x);
        assertEquals(-2,sub.y);
        
    }

    
    
    
    
    
}


