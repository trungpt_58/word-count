/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oop.vnu.demosamsung;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Trung Arsenal
 */
public class BankAccountTest {
  
   
    

    @Test
    public void TestBankAccount(){
    BankAccount account = new BankAccount("123","Nguyen Van A",10);
    assertEquals("123",account.getAccountNumber());
    
    }
    @Test
    public void testCreateAccount_NumberMustbeUnique(){
        
    }
    @Test
    public void testWithDraw_AmountTooMuch(){
        BankAccount account = new BankAccount("123"," Nguyen Van A", 10);
        account.withdraw(20);
        assertEquals(10,account.getBalance());
    }
    @Test
    public void testWithDrawShoulDecrease() {
        BankAccount account = new BankAccount("123"," Nguyen Van A", 10);
        account.withdraw(80);
    }
    @Test
    public void testTranfer(){
        BankAccount senderAccount = new BankAccount("123"," Pham Thi B",20);
    }
    @Test
    public void testInequality() {
        BankAccount a1 = new BankAccount("123", " Nguyen Van A", 10);
        BankAccount a2 = new BankAccount("12", " Nguyen Van A",10);
        assertEquals(a1,a2);// fail vì đây vẫn là 2 đối tượng khác nhau 
        // sẽ gọi hàm Equals
    }

}
