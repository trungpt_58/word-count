/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oop.vnu.demosamsung;

/**
 *
 * @author Trung Arsenal
 */
/**
 * @(#)Sv2.java
 *
 *
 * @author Thanh
 * @version 1.00 2011/7/7
 */
import java.util.Scanner;
import java.io.*;


public class Sv2 
{
    private String hoten;
    void setHoten(String hoten)
    {
    	this.hoten=hoten;
    }
    String getHoten()
    {
    	return hoten;
    }
	/*****************************/
	/***************************/
	void xem()
	{
	  System.out.print("\n\t\t"+hoten);	
	}
	
	void nhap()
	{
		Scanner nhap=new Scanner(System.in);
		System.out.print("\n Ho va ten: ");
		hoten=nhap.nextLine();
		setHoten(hoten);
	}
	/**********************************************************/
	
	public static String daoTen(String str) /*** VI DU VU TIEN THANH ->>> THANH  VU   TIEN ***/
    {                                                                /*   TEN->  HO->  DEM */ 
        String[] temp=str.split(" ");
        str ="";
        str=temp[temp.length-1]+" ";
	    for (int i=0;i<temp.length-1;i++)
	    {
            str=str + temp[i]+" ";
	    }
        return str;
    }
	/********************************************************/

	
    public static void main(String[] args)throws IOException {
        // TODO code application logic here
        Sv2 A[];
        int n;
        //Scanner sc=new Scanner(new FileInputStream(“filename.txt”),”UTF-8?)
 
        Scanner nhap=new Scanner(System.in);
        System.out.print("\n So sinh vien duoc nhap: ");
        n=nhap.nextInt();

        System.out.print("\n Nhap Danh sach sinh vien ");
        System.out.print("\n");
        A=new Sv2[n];  
        	             
        for(int i=0;i<n;i++)
        {
        	A[i]=new Sv2();
        	A[i].nhap();
        }
        
        /*** DAO TEN LEN DAU ROI SAP XEP THEO THU TU TRAI QUA PHAI ( TUC LA TEN-> HO-> DEM ) ***/
        
        String tenSvDao[]=new String[20];
        for(int i=0; i<n;i++)
        {
            tenSvDao[i]=daoTen(A[i].getHoten());
        }
        for(int i=0;i<n-1;i++)
        {
            for(int t=i+1;t<n;t++)
            {
                 if(tenSvDao[i].compareTo(tenSvDao[t]) >0)   
                 {
                    Sv2 temp;
                    temp=A[i];
                    A[i]=A[t];
                    A[t]=temp;
                 }
            }
        }
        
        /********* XEM **********/
        System.out.print("\n\n ********Danh sach Sinh vien********");
        System.out.print("\n\n\t      Ho va ten\n");
        for(int i=0;i<n;i++)
        {
           	 A[i].xem();
        }
        
    }
}
