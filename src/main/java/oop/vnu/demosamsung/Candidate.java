package oop.vnu.demosamsung;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Trung Arsenal
 */
public class Candidate {
    public  int index;
    public Candidate left;
    public Candidate right;

    Candidate(int index) {
        this.index = index;
       
    }
    
    
    public void printCircle() {
        Candidate currentCandidate = this;
        // lap
        // in index cua thang hien tai
        //chuyen sang thang tiep theo
        //cho den khi quay lai
        do{
            System.out.print(""+currentCandidate.index+ " ");
            currentCandidate = currentCandidate.left;
        }while( currentCandidate != this);
        System.out.println(".");
        
    }

    public int count(int myCount){
        
        //neu so dem cua toi la thu 3 thi di ra
        // va nguoi ben trai dem tiep 1
        // ket qua la ket qua cua count cua nguoi ben tra 
        //neu so dem cua toi <3 thi   
            // nguoi ben trai dem tiep so cua toi +1
            // det qua la ket qua count cua nguoi ben trai
        
        if(iAmTheLastOne() ) return index;// Nếu tôi là người cuối cùng thì trả về index của tôi
        if( myCount == 3){
             this.out();
             return this.left.count(1);
             
        }
        if(myCount <3){
             return this.left.count(myCount+1);
        }
        return 0;
        
        
    }
    
    private boolean iAmTheLastOne() {
        return ( this .left == this);
    }
   
    public void out(){
        // noi nguoi ben trai voi nguoi ben phai 
        this.left.right = this.right;
        this.right.left = this.left;
        System.out.println(index + " has left");
        this.left.printCircle();
    }
    
    
}
