/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oop.vnu.demosamsung;

/**
 *
 * @author Trung Arsenal
 */
public class DemoBankAccount {
    public static void main(String[] args) {
            BankAccount account1 = new BankAccount("1", "A",10);
            BankAccount account2 = new BankAccount("2","B",20);
            System.out.println("Tai khoan cua nguoi thu nhat "+account1.getOwer());
            System.out.println("So tai khoan la :"+account1.getAccountNumber());
            System.out.println("So du tai khoan la :"+ account1.getBalance());
            // dung phuong thuc toString();
            System.out.println(""+account1.toString());
        }
    
}
