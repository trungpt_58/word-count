/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oop.vnu.demosamsung;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Scanner;

/**
 *
 * @author Trung Arsenal
 */
public class DemoFile{
    String [] arr = {
            "a","á","à","ã","ạ",
            "ă","ắ","ằ","ẵ","ặ",
            "â","ấ","ầ","ẫ","ậ",
            "b","c","d","đ",
            "đ","é","è","ẽ","ẹ",
            "ê","ế","ề","ễ","ệ",
            "g","h","i","í","ì","ĩ","ị",
            "k","l","m","n","o",
            "ó","ò","õ","ọ","ô",
            "ố","ồ","ỗ","ộ","ơ",
            "ớ","ờ","ỡ","ợ",
            "p","q","r","s","t","u",
            "ú","ù","ũ","ụ","v","x","y"
        };
    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        FileReader pr = new FileReader("IN.txt");
        Scanner scanner = new Scanner(pr);
        //BufferedReader br = new BufferedReader(pr);
        //String line = " ";
        FileOutputStream fos = new FileOutputStream("output.txt", true);
        PrintWriter pw = new PrintWriter(fos);
        int count =0;
        String s ="";
        String t = "";
        int n =0;
        String[] a = new String[10000];
        
       
       while(scanner.hasNextLine()){
           System.out.println("  "+scanner.nextLine());
           s = s +scanner.nextLine().toString();
       }
       s+=" ";
       System.out.println(""+s);
       for( int i=0; i< s.length(); i++){
           if(s.charAt(i) !=' '){
               t = t +s.charAt(i);       
           }else{
               n++;
               a[n] = t;
               t = "";
           }
       }
       
       String tg = new String();
       for(int j=0; j<n-1; j++){
           for(int i=j+1; i<n; i++){
               if(a[i].compareTo(a[j]) !=0){
                   tg = a[j];
                   a[j] = a[i];
                   a[i]=tg;   
                   count++;
               }
           }
        }
       for(int i=0; i<=n; i++){
           System.out.println(""+a[i]);
       }
       
    }
    
    public int compareTo( String a, String b) {
        int u = 0;
        int v = 0;
        int t = Math.min(a.length(), b.length());
        int res =0;
        for( int i=0; i<arr.length; i++){
            if(arr[i]==a){
                i=u;
            }
        }
        for( int i=0; i<arr.length; i++){
            if(arr[i]==b){
                i=v;
            }
        }     
        for(int i=0; i<t ; i++ ){
            if( u>v) res = 1 ;
            if(u<v) res = -1;
            if(u==v) res = 0;
        }
        if(a.length() >b.length()) res = -1;
        if(a.length() < b.length()) res = 1;
        if(a.length() == b.length()) res = 0;
        return res;
        
    }
       


    protected static String readFile( String patchFile){
        try{
            FileInputStream fos = new FileInputStream(patchFile);
            Reader r = new InputStreamReader(fos,"UTF8");
            BufferedReader reader = new BufferedReader(r);
            StringBuilder text = new StringBuilder();
            String line= null;
            while((line=reader.readLine()) != null){
                text.append(line+"\n");
            }


            reader.close();
            return text.toString();

        }catch(Exception e){
            System.out.println("Không đọc được file :" +patchFile);
        }

     return "";   
    }
    
    
    protected static boolean writeFile(String patchFile, String text, boolean overWrite){
        try{
            String oldtext = "";
            if(overWrite)
                oldtext = readFile(patchFile);
            FileOutputStream fos = new FileOutputStream(patchFile);
            Writer out = new OutputStreamWriter(fos, "UTF8");
            out.close();
            return true;   
        }catch(Exception ex){
            System.out.println("Khong ghi duoc file :"+patchFile);
        }
        return false;
        
    }
    
   
}    

